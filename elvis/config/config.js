require('dotenv').config();
module.exports = {
  "development": {
    "username": process.env.database_user,
    "password": process.env.database_password,
    "database": process.env.database,
    "host": process.env.database_host,
    "dialect": process.env.database_dialect
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  // Later...
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
