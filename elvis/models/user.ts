'use strict';

import {
  Model
} from 'sequelize';

interface UserAttributes {
  userId: string, //Will use UUID which is a string
  userName: string,
  firstName: string,
  lastName: string,
  email: string,
  password: string
}

module.exports = (sequelize: any, DataTypes: any) => {
  class User extends Model<UserAttributes> 
  implements UserAttributes {
    
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    userId!: string;
    userName!: string;
    firstName!: string;
    lastName!: string;
    email!: string;
    password!: string;
    static associate(models: any) {
      // define association here
    }
  };
  User.init({
    userId: DataTypes.INTEGER,
    userName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};