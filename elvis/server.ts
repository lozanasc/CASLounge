/*
    * project coding style guidelines:
    ? const = snake_case
    ? classes and interfaces = PascalCase
    ? the rest like variables and functions are in camelCase
*/

import dotenv from 'dotenv';
import express from 'express';
import db from './models';

dotenv.config();

const api_server = express();
const port = process.env.PORT || 3000;

api_server.use(express.urlencoded({extended: true}));
api_server.use(express.json());

db.sequelize.sync().then(() => {
    api_server.listen(port, () => {
        console.log(`API server is listening at port = ${port}`);
    });
})
