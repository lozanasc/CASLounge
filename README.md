# CASLounge
A social networking platform for CAS students by CAS students.  
##### Codebases:
<table>
  <tr>
    <th>
      Codename
    </th>
    <th>
      Platform
    </th>
  <tr/>
  <tr>
    <td>
      <a href="https://github.com/lozanasc-projects/CSLounge/tree/main/kavis">kavis</a>
    </td>
    <td>
      Web
    </td>
  </tr>
  <tr>
    <td>
      <a href="https://github.com/lozanasc-projects/CSLounge/tree/main/yevis">yevis</a>
    </td>
    <td>
      Mobile
    </td>
  </tr>
  <tr>
    <td>
      <a href="https://github.com/lozanasc-projects/CSLounge/tree/main/elvis">elvis</a>
    </td>
    <td>
      API
    </td>
  </tr>
</table>
